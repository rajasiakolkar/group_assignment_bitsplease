/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.CustomerDirectory;
import Business.SupplierDirectory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author harshalneelkamal
 */
public class Admin extends User {
    
    public SupplierDirectory suppDir;
    public CustomerDirectory custDir;
    
    public Admin() {
        super("", "", "Admin");
        suppDir = new SupplierDirectory();
        custDir = new CustomerDirectory();
    }

    public SupplierDirectory getSuppDir() {
        return suppDir;
    }

    public void setSuppDir(SupplierDirectory suppDir) {
        this.suppDir = suppDir;
    }

    public CustomerDirectory getCustDir() {
        return custDir;
    }

    public void setCustDir(CustomerDirectory custDir) {
        this.custDir = custDir;
    }
    
    @Override
    public boolean verify(String password){
        
        String pass = "^[A-Za-z0-9_$+]+$";
        Pattern pattern = Pattern.compile(pass);
        Matcher matcher = pattern.matcher(password);
        
        if(matcher.matches())
            return true;
        return false;
    }
    
    @Override
    public boolean verifyUser(String username){
        String user = "^[^(?!_)][A-Za-z0-9+_.]+@(.+)$";
        Pattern pattern = Pattern.compile(user);
        Matcher matcher = pattern.matcher(username);
        
        if(matcher.matches())
            return true;
        return false;
    }
    
}
