/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author harshalneelkamal
 */
public class Customer extends User{

    private Date date;
    
    
    public Customer(String password, String userName) {
        super(password, userName, "CUSTOMER");
        date = new Date();
        
    }

    
    
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
   
    
    @Override
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }
    
    @Override
    public boolean verifyUser(String username){
        String user = "^[^(?!_)][A-Za-z0-9+_@]+$";
        Pattern pattern = Pattern.compile(user);
        Matcher matcher = pattern.matcher(username);
        
        if(matcher.matches())
            return true;
        return false;
    }
}
