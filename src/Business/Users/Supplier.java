/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.ProductDirectory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author harshalneelkamal
 */
public class Supplier extends User implements Comparable<Supplier>{
    
    private ProductDirectory directory;
    
    public Supplier(String password, String userName) {
        super(password, userName, "SUPPLIER");
        directory = new ProductDirectory();
    }

    public ProductDirectory getDirectory() {
        return directory;
    }

    public void setDirectory(ProductDirectory directory) {
        this.directory = directory;
    }

    @Override
    public int compareTo(Supplier o) {
        return o.getUserName().compareTo(this.getUserName());
    }

    @Override
    public String toString() {
        return getUserName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }
    
    @Override
    public boolean verifyUser(String username){
        String user = "^[^(?!_)][A-Za-z0-9+_@]+$";
        Pattern pattern = Pattern.compile(user);
        Matcher matcher = pattern.matcher(username);
        
        if(matcher.matches())
            return true;
        return false;
    }
    
}
